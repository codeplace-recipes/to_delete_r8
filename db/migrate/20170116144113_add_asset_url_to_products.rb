class AddAssetUrlToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :asset_url, :string
  end
end
